#!/bin/bash

has_dev_option=0
has_front_option=0
has_back_option=0
has_bus_option=0
has_bd_option=0

## Parsing inputs
for i in "$@"
do
case $i in
  -d) has_dev_option=1;;
  -f) has_front_option=1;;
  -bk) has_back_option=1;;
  -bs) has_bus_option=1;;
  -bd) has_bd_option=1;;
  -a) has_bus_option=1
	  has_front_option=1
	  has_back_option=1
	  has_bd_option=1
esac
done
if ([ $has_front_option -eq 0 ] && [ $has_back_option -eq 0 ] && [ $has_bus_option -eq 0 ] && [ $has_bd_option -eq 0 ]);
then 
printf "Aucun container à lancer.\n"
printf "Utiliser les options suivantes : -d pour le mode developpement, -f pour le container front, -bk pour le container back, -bs pour le container bus, -bd pour le container de base de données,\n"
else
source importcsv/PROJECTS/.env


## start the right version
if [ $has_dev_option -eq 1 ]; then
    version="dev"
else
    version="prod"
fi
export NODE_ENV=$version
docker_compose_params=
printf "La version $version est en train d'être lancée avec...\n"
if [ $has_front_option -eq 1 ];
then
printf "x L'image docker front suivante : $FRONT_IMAGE\n"
printf "	+ Le front se connectera au bus sur l'URL suivante : ${FRONT_BUS_URL}\n"
printf "	+ Le front se connectera au bus sur le port suivant : ${FRONT_BUS_PORT}\n"
printf "	+ Le front est disponible sur le port suivant : ${FRONT_PORT_3000}\n"
docker_compose_params="$docker_compose_params -f docker-compose.front.yml" 
fi
if [ $has_back_option -eq 1 ];
then
printf "x L'image docker back suivante : $BACK_IMAGE\n"
printf "	+ Les fichiers csv d'imports sont présents dans le dossier suivant : ${DATA_ORIENTIMPORTCSV}\n"
printf "	+ Les fichiers de backup sont présents dans le dossier suivant : ${DB_BACKUP}\n"
printf "	+ Le back se connectera au bus sur l'URL suivante : ${BACK_BUS_URL}\n"
printf "	+ Le back se connectera à la base de données sur l'URL suivante : ${BACK_BD_URL}\n"
printf "	+ Le nom de la base de données est le suivant : ${DB_NAME}\n"
printf "	+ L'utilisateur administrateur de la base de données est le suivant : ${DB_USER}\n"
printf "	+ Le mot de passe administrateur de la base de données est le suivant : ${DB_PASSWORD}\n"
printf "	+ Activation de la restauration à partir du backup : ${RESTORE_DB_FROM_BACKUP}\n"
printf "	+ Activation de la suppression récursive : ${BUSINESS_RECURSIVE_DELETION}\n"
printf "	+ Activation de l'historique : ${WITH_HISTORY}\n"
printf "	+ Utilisateur du serveur mail : ${MAIL_USERNAME}\n"
printf "	+ Mot de passe du serveur mail : ${MAIL_PASSWORD}\n"
printf "	+ Host du serveur mail : ${MAIL_HOST}\n"
printf "	+ Port du serveur mail : ${MAIL_PORT}\n"
printf "	+ Protocole du serveur mail : ${MAIL_PROTOCOL}\n"
printf "	+ Activation TLS du serveur mail : ${MAIL_TLS}\n"
printf "	+ Activation SMTP Auth du serveur mail : ${MAIL_SMTP_AUTH}\n"
printf "	+ Activation du démarrage TLS du serveur mail : ${MAIL_SMTP_STARTTLS_ENABLE}\n"
printf "	+ SSL trust du serveur mail : ${MAIL_SMTP_SSL_TRUST}\n"

docker_compose_params="$docker_compose_params -f docker-compose.back.yml"
if [ $has_dev_option -eq 1 ];
then
printf "	+ Le back est disponible sur le port suivant : ${BACK_PORT_8093}\n"
docker_compose_params="$docker_compose_params -f docker-compose.back.dev.yml"
fi
fi
if [ $has_bd_option -eq 1 ];
then
printf "x L'image docker base de données suivante : orientdb:3.0.28-tp3\n"
docker_compose_params="$docker_compose_params -f docker-compose.bd.yml"
printf "	+ Les fichiers de backup d'OrientDB sont disponibles dans le dossier suivant : ${DB_BACKUP}\n"
if [ $has_dev_option -eq 1 ];
then
printf "	+ L'interface de gestion d'OrientDB est disponible sur le port suivant : ${BD_PORT_2480}\n"
printf "	+ Les connexions binaires sur OrientDB sont disponibles sur le port suivant : ${BD_PORT_2424}\n"
docker_compose_params="$docker_compose_params -f docker-compose.bd.dev.yml"
fi
fi
if [ $has_bus_option -eq 1 ];
then
printf "x L'image docker bus suivante : rmohr/activemq\n"
printf "	+ Le port du bus connecté au back est le suivant : ${BUS_PORT_61616}\n"
printf "	+ Le port du bus connecté au front est le suivant : ${BUS_PORT_61613}\n"
printf "	+ L'interface de gestion du bus est disponible sur le port suivant : ${BUS_PORT_8161}\n"
docker_compose_params="$docker_compose_params -f docker-compose.bus.yml" 
fi
docker-compose $docker_compose_params up -d
fi
