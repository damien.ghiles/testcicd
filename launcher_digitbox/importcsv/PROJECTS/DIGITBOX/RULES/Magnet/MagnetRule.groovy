/**
 *  DIGITBOX BUSINESS RULE for Agent Magnet
 *  -> create Parking when necessary
 *
 */
package com.activus.digibrain.storage.digitbox.groovy.rules

import com.activus.digibrain.storage.domain.graph.AbstractGraphEntity;
import com.activus.digibrain.storage.domain.graph.GraphAgentEntity
import com.activus.digibrain.storage.groovy.rules.AbstractRule
import com.activus.digibrain.storage.groovy.rules.RulesException;
import com.activus.digibrain.storage.groovy.rules.AbstractRule.Operation;
import com.activus.digibrain.storage.groovy.rules.AbstractRule.Trigger;
import com.activus.digibrain.storage.service.graph.AbstractGraphService;
import com.activus.digibrain.storage.service.graph.GraphService
import com.activus.tools.SearchObject;
import com.orientechnologies.orient.core.db.ODatabaseSession
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.sql.executor.OResult
import com.orientechnologies.orient.core.sql.executor.OResultSet
import groovy.transform.CompileStatic

/**
 * Agent Magnet rule
 *
 */
@CompileStatic
public class MagnetRule extends AbstractRule{	
	
	/**
	 * execute a rule AFTER a SAVE operation
	 * @param graphService
	 * @param trigger
	 * @param op
	 * @param entity
	 * @param parameters
	 * @throws RulesException
	 */
	@Override
	public void executeRulePreSave(GraphService graphService, ODatabaseSession session, Trigger trigger, Operation op, AbstractGraphEntity entity, Map<String, Object> parameters) throws RulesException {
		String ruleName = 'MagnetRule';
		log.debug("execute ${ruleName} agent ${entity.get(AbstractGraphEntity.ENTITY_PROP_ELEMENT_CLASS_NAME)} in a SAVE operation");
		
		Object entityRID = entity.get(AbstractGraphEntity.ENTITY_PROP_RID);
		Object type = entity.get("type");
		Object label = entity.get((GraphAgentEntity.ABSTRACT_AGENT_PROP_LABEL));

		if(type != null && label != null) {
			
			if(isMagnetOftypeWithLabel(type.toString(), label.toString(), entityRID, session)) {
				String msg = "You cannot name the Magnet \""+label.toString()+"\" as there is already a Magnet of type \""+type.toString()+"\" with the same name";
				throw new RulesException(msg);
			}

		}
		

	}
	
	/***
	 * Return true if a Magnet with the same magnetType exist with the same name
	 * @param type
	 * @param label
	 * @param entityRID
	 * @return
	 */
	public boolean isMagnetOftypeWithLabel(String type, String  label, Object entityRID, ODatabaseSession session) {

		String request = "select from (select expand(in('LinkToMagnetType')) from magnetType where name='"+type+"' ) WHERE label='"+label+"' AND class instanceof Magnet";
		if(entityRID != null) {
			request+=" AND @rid !='"+entityRID.toString()+"'";
		}
		log.debug(request);
		OResultSet set = session.activateOnCurrentThread().execute("SQL", request);
		boolean res = set.hasNext();
		set.close();
		return (res) ;
	}
	
}
