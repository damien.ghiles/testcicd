/**
 *  DIGITBOX BUSINESS RULE for Agent WhiteBoard
 *  -> create Parking when necessary
 *
 */
package com.activus.digibrain.storage.digitbox.groovy.rules

import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import org.json.simple.parser.ParseException
import org.springframework.stereotype.Component
import com.activus.digibrain.storage.digitbox.model.Whiteboard
import com.activus.digibrain.storage.digitbox.model.Zone
import com.activus.digibrain.storage.domain.graph.AbstractGraphEntity;
import com.activus.digibrain.storage.domain.graph.GraphAgentEntity
import com.activus.digibrain.storage.domain.graph.GraphNodeEntity
import com.activus.digibrain.storage.domain.graph.GraphRelationEntity
import com.activus.digibrain.storage.graph.StorageGraphDb
import com.activus.digibrain.storage.groovy.rules.AbstractRule
import com.activus.digibrain.storage.groovy.rules.RulesException;
import com.activus.digibrain.storage.groovy.rules.AbstractRule.Operation;
import com.activus.digibrain.storage.groovy.rules.AbstractRule.Trigger;
import com.activus.digibrain.storage.service.graph.AbstractGraphService;
import com.activus.digibrain.storage.service.graph.GraphService
import com.activus.tools.SearchObject;
import com.orientechnologies.orient.core.db.ODatabaseSession
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.sql.executor.OResult
import com.orientechnologies.orient.core.sql.executor.OResultSet
import groovy.transform.CompileStatic

/**
 * Agent WhiteBoard rule
 *
 */
@CompileStatic
public class WhiteBoardRule extends AbstractRule{	
	
	/**
	 * execute a rule AFTER a SAVE operation
	 * @param graphService
	 * @param trigger
	 * @param op
	 * @param entity
	 * @param parameters
	 * @throws RulesException
	 */
	@Override
	public void executeRulePostSave(GraphService graphService, ODatabaseSession session, Trigger trigger, Operation op, AbstractGraphEntity entity, Map<String, Object> parameters) throws RulesException {
		String ruleName = 'WhiteBoardRule';

		log.debug("execute ${ruleName} agent ${entity.get(AbstractGraphEntity.ENTITY_PROP_ELEMENT_CLASS_NAME)} in a SAVE operation");
		Object magnetTypes =  entity.get("magnetTypes");
		String PREFIX = "Parking_";

		if(magnetTypes != null) {

			if(magnetTypes instanceof List) {

				for(Object type : magnetTypes) {

					String rid = getParkingRIDByType(type.toString(), session);
					
					if(rid == null) {

						//Create parking
						GraphNodeEntity parking = new GraphNodeEntity("Zone");
						parking.put(GraphAgentEntity.ABSTRACT_AGENT_PROP_NAME, PREFIX+System.currentTimeMillis());
						parking.put(GraphAgentEntity.ABSTRACT_AGENT_PROP_LABEL, type.toString());
						parking.put("isParking", true);
						ArrayList<String> listStr = new ArrayList<String>();
						listStr.add(type.toString());
						parking.put("magnetTypes", listStr);
						
						//save Parking	
						session.activateOnCurrentThread();
						parking = graphService.save(parking);
						rid = parking.getRid();
						
						//Link Parking to Magnet Type
						session.activateOnCurrentThread();
						GraphNodeEntity magnetType =rulesExecutor.getNodeService().findEntityByName("MagnetType", type.toString(), session);
						GraphRelationEntity newRelation = new GraphRelationEntity("LinkToMagnetType", rid, magnetType.getRid());
						session.activateOnCurrentThread();
						rulesExecutor.getRelationService().save(newRelation, session);
					}

					//Link Zone to Whiteboard
					if(!isZoneLinkedToWhiteBoard(rid, entity.getRid(), session)) {
						GraphRelationEntity link = new GraphRelationEntity("BelongsToWhiteboard");
						link.setSourceRid(entity.getRid());
						link.setDestRid(rid);
						session.activateOnCurrentThread();
						rulesExecutor.getRelationService().save(link, session);
					}
				}
			}else {
				log.error("magnetTypes is not an ArrayList");
			}
		}

	}
	
	/**
	 * Get Zone parking RID of the given magnetType
	 * @param magnetType
	 * @return
	 * @throws ParseException
	 */
	public String getParkingRIDByType(String magnetType, ODatabaseSession session) throws ParseException {

		String request = "select from Zone where isParking = true AND '"+magnetType+"' in magnetTypes";
		OResultSet set = session.execute("SQL", request);
		String rid = null;
		if (set.hasNext()) {
			OResult ores = set.next();
			String currentStr = ores.toJSON();
			rid = ((JSONObject) new JSONParser().parse(currentStr)).get("@rid");
		}
		set.close();
		return rid;
	}
	
	/**
	 * Return true if Zone is linked to whiteboard
	 * @param zoneRid
	 * @param whiteBoardRID
	 * @return
	 */
	public boolean isZoneLinkedToWhiteBoard(String zoneRid, String  whiteBoardRID, ODatabaseSession session) {

		String request = "select from BelongsToWhiteboard where (in='"+zoneRid+"' AND out='"+whiteBoardRID+"') OR (out='"+zoneRid+"'AND in= '"+whiteBoardRID+"')";
		OResultSet set = session.execute("SQL", request);
		boolean res = set.hasNext();
		set.close();
		return (res) ;
	}
	
}
