/**
 *  SAMPLE Function
 *  Select all agents hte specified category
 *  language: groovy
 *  
 *  parameters:
 *    category: required category
 *
 */

def req = "select from ${category}"
log.info(req) 
return db.query(req)