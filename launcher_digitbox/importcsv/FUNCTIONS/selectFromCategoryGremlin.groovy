/**
 *  SAMPLE Function
 *  Select all agents hte specified category
 *  language: groovy-gremlin
 *
 *  parameters:
 *    category: required category
 *
 */

log.info(category)
return g.V().hasLabel("'${category}'").valueMap()
