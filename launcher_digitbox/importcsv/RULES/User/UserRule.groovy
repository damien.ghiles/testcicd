/*
 *
 *  *  Copyright 2020 Activus Group (info(at)activus-group.com)
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *
 *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *  *
 *  * For more information: http://www.activus-services.com
 *
 */
package com.activus.digibrain.storage.groovy.rules;                                                                                                                                                                                                                                                                                                                                                                       
import com.activus.digibrain.storage.domain.graph.AbstractGraphEntity;
import com.activus.digibrain.storage.domain.graph.GraphOUserEntity
import com.activus.digibrain.storage.groovy.rules.AbstractRule.Operation;
import com.activus.digibrain.storage.groovy.rules.AbstractRule.Trigger;
import com.activus.digibrain.storage.service.graph.AbstractGraphService;
import com.activus.digibrain.storage.service.graph.GraphService;
import com.activus.digibrain.storage.service.graph.GraphOUserService;
import com.orientechnologies.orient.core.db.ODatabaseSession
import com.orientechnologies.orient.core.metadata.security.ORole
import com.orientechnologies.orient.core.metadata.security.OUser
import com.orientechnologies.orient.core.record.OElement
import com.orientechnologies.orient.core.record.impl.ODocument
import com.orientechnologies.orient.core.sql.executor.OResult
import com.orientechnologies.orient.core.sql.executor.OResultSet
import com.activus.tools.SaveEntity;

import groovy.transform.CompileStatic
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
/**
 * User rule
 * @author Amandine Jacquelin
 *
 */
class UserRule extends AbstractRule{
	/**
	 * execute a rule AFTER a SAVE operation
	 * @param graphService
	 * @param session
	 * @param trigger
	 * @param op
	 * @param entity
	 * @param parameters
	 * @throws RulesException
	 */
	@Override
	public void executeRulePostSave(GraphService graphService, ODatabaseSession session, Trigger trigger, Operation op, AbstractGraphEntity entity, Map<String, Object> parameters) throws RulesException {
		String ruleName = 'CreateOUserRule';
		log.info("execute ${ruleName} agent ${entity.get(AbstractGraphEntity.ENTITY_PROP_ELEMENT_CLASS_NAME)} in a SAVE operation");
		
		//Save OUser
		GraphOUserEntity ouser = rulesExecutor.graphUserService.userToOUser(entity);
		rulesExecutor.graphUserService.save(new SaveEntity(ouser), session);
		
		//Do not write password on User class
		OElement element = rulesExecutor.graphUserService.entityToElement(entity, session);
		element.setProperty(GraphOUserEntity.OUSER_PROP_PASSWORD,"");
		element.save();
	}
	
	/**
	 * execute a rule AFTER a DELETE operation
	 * @param graphService
	 * @param session
	 * @param trigger
	 * @param op
	 * @param entity
	 * @param params can be null
	 * @throws RulesException
	 */
	@Override
	public void executeRulePostDelete(GraphService graphService, ODatabaseSession session, Trigger trigger, Operation op, AbstractGraphEntity entity, Map<String, Object> parameters) throws RulesException {
		String ruleName = 'DeleteOUserRule';
		GraphOUserEntity ouser = rulesExecutor.graphUserService.getOUserByUser(entity.getRid());
		rulesExecutor.graphUserService.delete(ouser.getRid(), session);
	}
	
	
	

	@Override
	public void executeRulePreSave(GraphService graphService, ODatabaseSession session, Trigger trigger, Operation op, AbstractGraphEntity entity, Map<String, Object> parameters) throws RulesException {
		String ruleName = 'checkUserMailRule';
		log.debug("execute ${ruleName} agent ${entity.get(AbstractGraphEntity.ENTITY_PROP_ELEMENT_CLASS_NAME)} in a PRE SAVE operation");
		if(!isMailUnique(entity.get(GraphOUserEntity.OUSER_PROP_MAIL), session, entity.getRid())) {
			String msg = "Mail is already in use";
			throw new RulesException(msg);
		}
	}
	
	
	/**
	 * When a mail is set on a User, check it's not used by anyone else
	 * @param saveEntity
	 * @return
	 */
	public boolean isMailUnique(Object mail, ODatabaseSession session, String rid) {
		boolean res = true; 

		if(mail != null) {

			String request = "SELECT FROM User WHERE mail = \"" + mail.toString() + "\"";
			OResultSet set = session.execute("SQL", request);

			if (set.hasNext()) {
				OResult ores = set.next();
				String currentStr = ores.toJSON();
				String ridFound = ((JSONObject) new JSONParser().parse(currentStr)).get("@rid");
				if(rid == null || !ridFound.equals(rid)) {
					res = false;
				}
			}
			set.close();
		}

		return res;
	}
	

}
