/*
 *
 *  *  Copyright 2020 Activus Group (info(at)activus-group.com)
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *
 *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *  *
 *  * For more information: http://www.activus-services.com
 *
 */
package com.activus.digibrain.storage.groovy.rules;                                                                                                                                                                                                                                                                                                                                                                       
import com.activus.digibrain.storage.domain.graph.AbstractGraphEntity;
import com.activus.digibrain.storage.domain.graph.GraphORoleEntity
import com.activus.digibrain.storage.groovy.rules.AbstractRule.Operation;
import com.activus.digibrain.storage.groovy.rules.AbstractRule.Trigger;
import com.activus.digibrain.storage.service.graph.GraphService;
import com.activus.digibrain.storage.service.graph.AbstractGraphService;
import com.activus.digibrain.storage.service.graph.GraphORoleService;
import com.activus.tools.SaveEntity
import com.orientechnologies.orient.core.db.ODatabaseSession
import com.orientechnologies.orient.core.metadata.security.ORole

import groovy.transform.CompileStatic
/**
 * Role rule
 * @author Amandine Jacquelin
 *
 */
class RoleRule extends AbstractRule{
	/**
	 * execute a rule AFTER a SAVE operation
	 * @param graphService
	 * @param session
	 * @param trigger
	 * @param op
	 * @param entity
	 * @param parameters
	 * @throws RulesException
	 */
	@Override
	public void executeRulePostSave(GraphService graphService, ODatabaseSession session, Trigger trigger, Operation op, AbstractGraphEntity entity, Map<String, Object> parameters) throws RulesException {
		String ruleName = 'CreateORoleRule';
		log.info("execute ${ruleName} agent ${entity.get(AbstractGraphEntity.ENTITY_PROP_ELEMENT_CLASS_NAME)} in a SAVE operation");
		GraphORoleEntity orole = rulesExecutor.graphRoleService.roleToORole(entity, session);
		rulesExecutor.graphRoleService.save(new SaveEntity(orole), session);
	}
	
	/**
	 * execute a rule AFTER a DELETE operation
	 * @param graphService
	 * @param session
	 * @param trigger
	 * @param op
	 * @param entity
	 * @param params can be null
	 * @throws RulesException
	 */
	@Override
	public void executeRulePostDelete(GraphService graphService, ODatabaseSession session, Trigger trigger, Operation op, AbstractGraphEntity entity, Map<String, Object> parameters) throws RulesException {
		String ruleName = 'DeleteORoleRule';
		GraphORoleEntity orole = rulesExecutor.graphRoleService.getORoleByRole(entity.getRid(), session);
		rulesExecutor.graphRoleService.delete(orole.getRid(), session);
	}
}
