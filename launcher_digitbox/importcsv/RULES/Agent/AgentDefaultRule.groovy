/*
 *
 *  *  Copyright 2018 Activus Services (info(at)activus-services.com)
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *
 *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *  *
 *  * For more information: http://www.activus-services.com
 *
 */
package com.activus.digibrain.storage.groovy.rules

import com.activus.digibrain.storage.domain.graph.AbstractGraphEntity
import com.activus.digibrain.storage.domain.graph.GraphAgentEntity
import com.activus.digibrain.storage.groovy.rules.AbstractRule.Operation
import com.activus.digibrain.storage.groovy.rules.AbstractRule.Trigger
import com.activus.digibrain.storage.service.graph.GraphService
import com.orientechnologies.orient.core.db.ODatabaseSession

/**
 * Agent default rule
 * > Verify that the agent name is compliant
 *
 */
class AgentDefaultRule extends AbstractRule {
	
	private final static PERSONNE_CATEGORY_NAME = "Personne";

	/**
	 * execute a rule BEFORE a SAVE operation 
	 * @param graphService
	 * @param trigger
	 * @param op
	 * @param entity
	 * @param parameters
	 * @throws RulesException
	 */
	@Override
	public void executeRulePreSave(GraphService graphService, ODatabaseSession session, Trigger trigger, Operation op, AbstractGraphEntity entity, Map<String, Object> parameters) throws RulesException {
		String ruleName = 'checkName';

		log.info("execute ${ruleName} agent ${entity.get(AbstractGraphEntity.ENTITY_PROP_ELEMENT_CLASS_NAME)} in a SAVE operation");

		if ( ((String)entity.get(GraphAgentEntity.ABSTRACT_AGENT_PROP_NAME)).isEmpty() && !(entity.get(AbstractGraphEntity.ENTITY_PROP_ELEMENT_CLASS_NAME).equals(PERSONNE_CATEGORY_NAME)) ) {
			String msg = getMessage( graphService, "RULE_MSG_ENTITY_NAME_CANNOT_BE_EMPTY");
			throw new RulesException(msg);
		}
	}
}