/*
 *
 *  *  Copyright 2020 Activus Group (info(at)activus-group.com)
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *
 *  *       http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *  *
 *  * For more information: http://www.activus-services.com
 *
 */
package com.activus.digibrain.storage.groovy.rules;
import java.util.HashSet;
import java.util.ArrayList;
import com.activus.digibrain.storage.domain.graph.AbstractGraphEntity;
import com.activus.digibrain.storage.domain.graph.GraphORoleEntity
import com.activus.digibrain.storage.domain.graph.GraphOUserEntity
import com.activus.digibrain.storage.domain.graph.GraphAgentEntity
import com.activus.digibrain.storage.domain.graph.GraphRelationEntity
import com.activus.digibrain.storage.groovy.rules.AbstractRule.Operation;
import com.activus.digibrain.storage.groovy.rules.AbstractRule.Trigger;
import com.activus.digibrain.storage.service.graph.AbstractGraphService;
import com.activus.digibrain.storage.service.graph.GraphService;
import com.activus.digibrain.storage.service.graph.GraphORoleService;
import com.activus.tools.SaveEntity
import com.orientechnologies.orient.core.db.ODatabaseSession
import com.orientechnologies.orient.core.metadata.security.ORole

import groovy.transform.CompileStatic
/**
 * HasRightTo rule
 * @author Amandine Jacquelin
 *
 */
class HasRightToRule extends AbstractRule{
	/**
	 * execute a rule AFTER a SAVE operation
	 * @param graphService
	 * @param session
	 * @param trigger
	 * @param op
	 * @param entity
	 * @param parameters
	 * @throws RulesException
	 */
	@Override
	public void executeRulePostSave(GraphService graphService, ODatabaseSession session, Trigger trigger, Operation op, AbstractGraphEntity entity, Map<String, Object> parameters) throws RulesException {
		String ruleName = 'CreateLinkORoleOUserRule';
		log.info("execute ${ruleName} agent ${entity.get(AbstractGraphEntity.ENTITY_PROP_ELEMENT_CLASS_NAME)} in a SAVE operation");
		// Get relation informations
		AbstractRule.RelationInfos infos = getRelationInfos(graphService, (GraphRelationEntity) entity, session);

		// Check if the link is between user and domainRole
		if (infos.destCategoryName.equals(GraphAgentEntity.DOMAIN_ROLE_CLASS_NAME)) {
			GraphORoleEntity orole = rulesExecutor.graphRoleService.getORoleByRole((
					rulesExecutor.graphDRService.getRole((GraphAgentEntity)infos.destEntity, session).getRid()), session);
				
			GraphOUserEntity ouser = rulesExecutor.graphUserService.getOUserByUser(infos.srcEntity.getRid());

			// Add orole to the ouser
			Set<String> roles = (Set<String>) ouser.get(GraphOUserEntity.OUSER_PROP_ROLES);
			String roleName = (String) orole.get(GraphAgentEntity.ABSTRACT_AGENT_PROP_NAME);
			if(!roles.contains(roleName)) {
				roles.add(roleName);
				ouser.put(GraphOUserEntity.OUSER_PROP_ROLES, new HashSet<>(roles));
				// Save ouser
				rulesExecutor.graphUserService.save(new SaveEntity(ouser), session);
			}
		}
	}
}
