#!/bin/bash

## Default behaviour (when no flags set)
has_front_option=0
has_back_option=0
has_bus_option=0
has_bd_option=0

## Parsing inputs
for i in "$@"
do
case $i in
  -f) has_front_option=1;;
  -bk) has_back_option=1;;
  -bs) has_bus_option=1;;
  -bd) has_bd_option=1;;
  -a) has_bus_option=1
	  has_front_option=1
	  has_back_option=1
	  has_bd_option=1
esac
done
if ([ $has_front_option -eq 0 ] && [ $has_back_option -eq 0 ] && [ $has_bus_option -eq 0 ] && [ $has_bd_option -eq 0 ]);
then 
printf "Aucun container à arrêter."
printf "Utiliser les options suivantes : -f pour le container front, -bk pour le container back, -bs pour le container bus, -bd pour le container de base de données,\n"
else
source importcsv/PROJECTS/.env

#export NODE_ENV=NODE_ENV
docker_compose_params=
printf "Arrêt des containers...\n"
if [ $has_front_option -eq 1 ];
then
docker_compose_params="$docker_compose_params -f docker-compose.front.yml" 
fi
if [ $has_back_option -eq 1 ];
then
docker_compose_params="$docker_compose_params -f docker-compose.back.yml"
fi
if [ $has_bd_option -eq 1 ];
then
docker_compose_params="$docker_compose_params -f docker-compose.bd.yml"
fi
if [ $has_bus_option -eq 1 ];
then
docker_compose_params="$docker_compose_params -f docker-compose.bus.yml" 
fi
docker-compose $docker_compose_params stop
fi