#!/bin/sh
#
# Gitlab script used in CICD to reset database
#
# To use:
#
#   * ./reset-database.sh
#
# Features:
#
#   * Remove database
#

DIR="/home/ec2-user/data"
if [ -d "$DIR" ]; then
  sudo rm -R $DIR;
  echo "${DIR} has been deleted";
else
  echo "${DIR} doesn't exist, nothing to do";
fi

