#!/bin/sh
#
# Script used in CICD to reset and init launcher sources on an aws distant machine.
#
# To use:
#
#   * ./reset_and_init_launcher.sh -b connectit -l develop -d develop
#
# Features:
#
#   * Reset and init launcher sources
#

git_launcher="develop"
git_data="develop"
business="connectit"

# Get command parameters
while getopts "b:l:d" opt; do
    case $opt in
        b) business="${OPTARG}";;
        l) git_launcher="${OPTARG}";;
        d) git_data="${OPTARG}";;
    esac
done

cd /home/ec2-user/
sudo rm -R launcher

 git clone --branch ${git_launcher} git@gitlab.com:activus-group/digibrain/launcher.git
 git clone --branch ${git_data} git@gitlab.com:activus-group/${business}/data.git launcher/importcsv/PROJECTS

