#!/bin/sh
#
# Gitlab script used in CICD to install aws client
#
# To use:
#
#   * ./install_awscli.sh
#
# Features:
#
#   * Install aws cli and display version
#

sudo apt-get install -y unzip
curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
unzip awscli-bundle.zip
sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
aws --version
